from flask import Flask, render_template, request, redirect
from random import choice
import psycopg2

app = Flask(__name__)

conn = psycopg2.connect("host=ec2-107-20-167-11.compute-1.amazonaws.com dbname=d5jgbfdk28op6a user=dkuzabxxepreua password=b913baa3ecc99536192c32dae363f865225ad183379baeea53e526b91b34e39e")
cur = conn.cursor()

quotes = ["The grass is greener where you water it.", "Bloom where you are planted.",
          "Comparison is the thief of joy.", "What you do not want done to yourself, do not do to others.", "The only joy in the world is to begin.", "Time you enjoy wasting is not wasted time."]
quote = choice(quotes)

@app.route('/')
def index():
    return render_template("index.html", quote=quote)
    
@app.route('/about')
def about():
    return render_template("about.html")

@app.route('/blog')
def blog():
    return render_template("blog.html")

@app.route('/contact', methods = ['POST', 'GET'])
def contact():
    cur.execute("SELECT * from submissions")
    return render_template("contact.html", hasil = cur.fetchall())

@app.route("/submit", methods=["POST"])
def submit():
    cur.execute("INSERT INTO submissions VALUES (%s, %s)", (request.form['sender'], request.form['message']))
    cur.execute("COMMIT")
    return redirect("/contact")
    
if __name__ == "__main__":
    app.run(debug=True)
